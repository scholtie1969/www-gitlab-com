---
layout: markdown_page
title: Support Handbook
---

## Welcome to the GitLab Support Handbook
{: .no_toc}

**Looking for technical support? Please visit the [Support Page](/support/) instead.**
{:.no_toc}

----

### On this page
{:.no_toc}

- TOC
{:toc}

## What does the Support Team do?

GitLab Support provides technical support to our Self-managed and GitLab.com customers for the GitLab product.

We *aren't* internal IT Support, so we probably can't help you with your MacBook or 1Password. (Check in with `#peopleops` for who to go to for these problems!)

If you're looking for some help as a team member, please see the [internal support for GitLab team members section](#internal-support-for-gitlab-team-members).

If you're a customer (or advocating for a customer), you should take a look at the dedicated [Support Page](/support) that describes how to get in
contact with us.

## <i class="fas fa-question-circle fa-fw icon-color font-awesome" aria-hidden="true"></i>  Support Direction

The overall direction for Support in 2019 is set by our overall [strategic objectives](/strategy), with a particular emphasis on
- continued improvement of (Premium) Customer satisfaction.
- scaling the team through hiring and process improvements to pace ticket growth
- refining process, communication and team-training to improve efficiency as we scale

### Focal Points of 2019
As can also be inferred from our [publicly visible OKR page](/company/okrs/), we anticipate 2019 will focus on the following elements:

#### Ticket deflection through documentation
{: .no_toc}

1. By building up a corpus of documentation informed by real-world problems we can ensure that customers can get the answers they need before they
come into the queues.
   - *When learning, use the docs. When troubleshooting, use the docs. If something is missing, update the docs.*

1. By developing a documentation-first approach to answering we can ensure that customers are aware of where to find things in documentation.
   - *Always respond with a link to the docs. If docs are missing, create it and link the customer to the MR.*

#### Increase capacity & develop experts
{: .no_toc}

In the past 3 years we expanded the support team significantly, and this trend will continue in 2019. As GitLab -- the product -- continues to expand, so will the skill and knowledge of all Support Engineers to be able to continue providing an excellent customer support experience.

1. Develop "Deep Dives", Bootcamps and other professional training programs to decrease the time it takes for a new team member to be effective.
1. Make sure that professional training sessions result in the production of publicly consumable resources:
docs fixes, tutorials and improvements to any related material.

#### Experiment with support model
{: .no_toc}

In late 2018 we moved from a "product-first" model to a "region-first" model. While this has affected the reporting structure, the lines
of work from the perspective of an individual Support team member continue to be aligned to product. In 2019 we want to experiment with a "concept-first"
model of support that will align individual strengths with customers needs regardless of which queue they come into.

Other areas we'd like to explore:
1. 24x7 capability beyond Emergency support (i.e. weekend staffing)
1. Language support
1. US-only Federal Support

### Hazards

#### Team morale suffers because:
{: .no_toc}

| Hazard                     | Commitment                              |
|----------------------------|-----------------------------------------|
| Ticket volume is too high | Build (and adjust) hiring model based on the best data available, hire as quickly as possible while keeping quality up. |
| Team knowledge doesn't match ticket difficulty | Develop training materials, provide time for professional development, encourage specializations.  |
| We aren't hitting our SLOs | Hire and train as per above. Add regularity to scheduling and encourage efficient work patterns.
| Leadership breaks trust    | Communicate in public channels, alert early and often of potential discussions, engender the GitLab value of Transparency. |
| Intra-team trust suffers   | Discussion on-going in [support-team-meta#1447](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1447)|
| Fear of conflict results in poor decisions | Provide focus on meta-issues by triaging issues in the [Active Now](#improving-our-processes---active-now-issue-board) board. Encourage buy-in and bravery. Truly listen and respond. Explicitly overrule: state reasoning and thank everyone for voicing opinions. |
| Team lacks commitment to results or implementing decisions | Ensure voices are heard. Re-enforce "disagree and commit". Build accountability. |
| There's no accountability in poor results or not meeting commitments | Reinforce GitLab value of Results by paying attention and following up. |

#### Scaling
{: .no_toc}

As we continue to increase in size, there's a number of challenges that we need to address and/or questions we need to answer:

As we grow:
1. What process inefficiencies do we need to address? (e.g. _Do all engineers need to know about every component/configuration/deployment strategy of GitLab, or are there ways we can specialize?_)
1. Which process inefficiencies do we need to intentionally keep because of a positive externality? (e.g. _Do we keep around a synchronous meeting because it helps build relationships?_)
1. What have we lost in scaling? How can we build it back in in a way that scales? (e.g. _Smaller groups lead to more ideation sessions, how can we make sure we're creating spaces for ideas to build off of each other?_)
1. How can we make sure we're broadcasting things appropriately so no one misses out on an important piece of information?


Know someone who might be a great fit for our team? Please refer them to the job descriptions below.
- [Support Engineer - Self Managed job description](/job-families/engineering/support-engineer).
- [Support Agent - GitLab.com job description](/job-families/engineering/dotcom-support/).

**Increase Documentation Contributions and Usage in Support Cases**

Support engineers should always reply to customer issues with a link, whether to
GitLab's existing documentation or to an MR that the support engineer creates with
new or updated content needed in the docs to address the issue. This [Docs-First methodology](/handbook/documentation/#docs-first-methodology)
is essential for ensuring that the documentation remains a highly useful
[single source of truth](/handbook/documentation/#documentation-as-single-source-of-truth-ssot).
It is not possible or practical to find content across multiple sources including
the Forum, public answers, internal guides, etc., while keeping such disparate sources up to date.

Beyond this work during specific support cases, the team is also asked to contribute their
expertise to GitLab's documentation by producing new guides and videos that would
be useful to our users — especially on emerging DevOps trends and use cases.

Improving the docs enables more users to help themselves, reducing unnecessary support cases.

**Actionable Analytics**

Better reporting on ticket metrics (topic, response times, product it relates to, etc.) will help guide the Support Team's priorities in terms of adding to the greatest documentation needs, bug fixes, and support processes.

## Contacting Support

### Internal Support for GitLab team members

Fellow GitLab team members can reach out for help from the Support Team in various ways:

1. For normal support questions ("Can GitLab do x?", "How do I do y with GitLab?") try:
   - posing your question on the `#questions` channel in Slack, so that everyone can contribute to an answer. If you're not getting an answer, try cross-posting in the [relevant support team channel](#support-chat-channels).
1. For longer term or larger scope questions, such as process change, or data gathering requests, create a [support issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues)
1. If you have a user or account request that needs a GitLab.com administrator, please create an issue in the [dotcom internal requests tracker](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal).
1. If customers or users have questions, advise them to contact support directly via the [support web form](https://support.gitlab.com).
1. As a last resort, ping the support team on one of the [support channels](#support-chat-channels).

### Support Issue Tracker

The [Support project](https://gitlab.com/gitlab-com/support/support-team-meta/issues) hosts an issue tracker meant to improve our workflow by reporting any problems that may arise in our tools or processes. It's also meant to propose and discuss ideas in general.

The issue tracker is open to the community and part of the GitLab.com group. It should not contain any sensitive information. Links to Zendesk or other references are encouraged.

### Support Chat Channels

The support channels are as follows:

- [#questions](https://gitlab.slack.com/messages/questions) - If your question is something that you think anyone in the company could answer or is valuable to the greater company to know, ask it here!
- [#zd-self-managed-feed](https://gitlab.slack.com/messages/C1CKSUTL5/) - Feed of all self-managed Zendesk ticket activities.
- [#zd-gitlab-com-feed](https://gitlab.slack.com/messages/CADGU8CG1/) - Feed of all GitLab.com Zendesk ticket activities.
- [#support-managers](https://gitlab.slack.com/messages/CBVAE1L48/) - This channel is specifically for support managers.
- [#support_self-managed](https://gitlab.slack.com/messages/support_self-managed/) - This channel is specifically for the self-managed support team. They handle self-managed production issues, triage bugs, and self-managed emergencies, among other things.
- [#support_gitlab-com](https://gitlab.slack.com/messages/C4XFU81LG/) - This channel is specifically for the GitLab.com support team. They handle GitLab.com account and subscription support and GitHost.
- [#githost](https://gitlab.slack.com/messages/githost/) - This channel handles monitoring for GitHost instances.

In order to attract support team's attention in Slack, you can use the team handles, mentioning multiple team members in a message or a thread where support is needed. Support team handles are:
- `@support-selfmanaged` - Self-managed support team members.
- `@support-dotcom` - GitLab.com support team members.
- `@supportmanagers` - Support director and managers.

## Our Meetings

Support has several meetings each week. These allow us to coordinate and help us all grow together. Each meeting has its own agenda and is led by a different member of the team each week.

|  Weekday  |   Region  |      Meeting Name     |                                        Purpose                                      |
|:---------:|:---------:|:---------------------:|:-----------------------------------------------------------------------------------:|
|  Tuesday  |    APAC   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |
|  Tuesday  |    AMER   |  Casual Catch up |         Agendaless, a space for the team to banter and chat. If you have work to talk about, feel free.        |
| Wednesday | AMER,EMEA | Dotcom Support Call | GitLab.com support team meeting to discuss metrics, demos, upcoming events, and ask questions |
|  Thursday  |    EMEA   |      Support Call     |              Discuss metrics, demos, upcoming events, and ask questions             |

The regions listed above are the regions for which each call may be the most convenient, but all are welcome on any call. Every call is recorded and notes are taken on the agenda for each one. If you miss a meeting or otherwise can't make it, you can always get caught up.

All Zoom and agenda links can be found on the relevant calendar entry in the Support Calendar.

### Role of the Notetaker
The notetaker should take notes during the meeting and if action is required, creates a comment and assigns it to the appropriate person.

If the notetaker is not available when it is their turn, they should find a substitute.

### Role of the Chair
The main role of the chair is to start the meeting, keep the meeting moving along, and end the meeting when appropriate. There is generally little preparation required, but depending on the meeting, will include choosing a "feature of the week" or similar. Please check the agenda template for parts marked as "filled in by chair."

During the meeting, the chair:

* will ensure that each point in the agenda is covered by the listed person,
* may ask the team to move a discussion to a relevant issue when appropriate,
* cover [metrics in brief](#weekly-metrics-in-brief), and
* copy the agenda template for the following week and tag the next chair/secretary.

If a chair is not available, it is their responsibility to find a substitute.

### Support Week in Review

As per issue [support-team-meta#1394](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1394) the team is large enough
that we are going to do a Friday week in review inspired by greater Engineering. You can add topics any time to the [support week in review google document](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit).

We want to think "issue first" so the majority of the doc should be issues, but also consider
links to cool articles you've found, or awesome pics from recent trips. It should be something like
a 60% work/ 40% cool stuff blend. We define our culture and this doc is no exception.

On Fridays at 3pm EST, slackbot will remind the team with a link to the week in review and we can use a thread there for banter.


### Improving our processes - 'Active Now' issue board
The Support team use ['support-team-meta' project issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues/) to track ideas and initiatives to improve our processes. The ['Active Now' issue board](https://gitlab.com/gitlab-com/support/support-team-meta/boards/580661) shows what we're currently working on. It uses three labels:

1. **Blocked** - waiting for another team or external resource before we can move ahead
1. **Discussing this week** - under active discussion to arrive at a decision
1. **In Progress** - actively being worked on

Some principles guide how these labels are used:

1. A **maximum of six issues** at any time for each label (18 total issues)
1. All issues with one of the above labels must be **assigned** to one or more support team members
1. All issues with one of the above labels must have a **due date** no longer than a week ahead
1. If an issue is too big to complete in a week it should be **split into smaller parts that can be completed in a week** (a larger 'parent' issue is OK to keep in the project, but it shouldn't make it onto the 'In Progress' column)

**Each week we look at the board and discuss the issues to keep things moving forward.**

By keeping a maximum of six issues for each label, we **limit work in progress** and make sure things are completed before starting new tasks.

**Adding and managing items on the board:**

Support managers will regularly review the board to keep items moving forward.

1. The team can **vote on issues not on the board** by giving a 'thumbs up' emoji so we can see [popular issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues?sort=popularity&state=opened).
1. Support managers will look at popular issues and add them to the board when there is room.
1. Support managers will **curate** issues to prevent a large backlog. Unpopular or old issues can be closed / merged to keep the backlog manageable.

### Weekly Metrics in Brief
The links to the relevant Zendesk views are in the agenda template, but can also be found under the `Reporting` tab in Zendesk. Two areas to cover:

1. Meeting our CSAT OKR?
1. Meeting our SLA OKR?

Answers to questions, deeper insight, and analysis is not expected of the chair. Support Managers will cover metrics in more depth as needed.

If you're presenting metrics for the first time and want to see how it is done, please see [past recordings of our meetings](https://drive.google.com/drive/u/0/folders/0B5OISI5eJZ-DcnBxS05QUTdyekk).

## Cross Functional Meetings
As a result of our direct interactions with customers, the Support Team occupies a unique position in GitLab. As such we have standing meetings
across functions to help surface issues and smooth the interface between groups. If you're missing from this list (and want to be on it)
please let the Support Managers know in `#support-managers`

If you're on the Support Team and have something you'd like to surface, or would like to attend one of these meetings, feel free to post in `#support-managers`.


| Function   | Party            | Support Rep         | Frequency     |
|:----------:|:----------------:|:-------------------:|:-------------:|
| Production | Dave Smith       | Lyle Kozloff        | every 2 weeks |
| Product    | Jeremy Watson    | Lyle Kozloff        | every 2 weeks |
| UX         | Taurie Davis     | Lyle Kozloff        | every 1 week  |
| PeopleOps  | Jessica Mitchell | Lee Matos           | every 2 weeks |
| Finance    | Chase Wright     | Tom Cooney          | 1x Qtr on budget + once per month |
| PeopleOps  | Jessica Mitchell | Tom Cooney          | every 2 weeks |
| Recruiting | Nadia Vatalidis  | Tom Cooney          | weekly |
| Docs       | Docs Team        | Tom Atkins          | every 2 weeks join docs meeting |
| Sales + CS | EMEA Sales/CS    | Tom Atkins          | weekly on Fri join EMEA scrum |


## Time Off
The Support Team follows [GitLab's paid time off policy](/handbook/paid-time-off). However, do note that if a large number of people are taking the same days off, you may be asked to reschedule. If a holiday is important to you, please schedule it early.

In addition to the tips in [Communicating Your Time Off](/handbook/paid-time-off/#communicating-your-time-off) please also:
- add an event to the [**Support Time-off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) calendar with your name and a brief note, marked "free"
- ensure that you take steps to alert your availability in Calendly (either in the Calendly app, or by making meetings marked "busy" during your working hours)
- if you're on-call, make sure you have coverage and that PagerDuty is up to date
- reschedule any 1:1 meetings with your manager
- add an agenda item during the team meeting before your time off
- add an agenda item after you get back to let us know what you've been up to!

If you do not have access to the [**Support Time-off**](https://calendar.google.com/calendar/embed?src=gitlab.com_as6a088eo3mrvbo57n5kddmgdg%40group.calendar.google.com) team calendar, please raise it in the `#support-team-chat` channel on Slack and someone will share it with you.

You do **not** need to add time-off events to the shared calendar if you'll be gone less than half a day. *Do* consider blocking off your
own calendar so that customer calls or other meetings won't be scheduled during your time away.

If you need to go for a run, grab a coffee or take a brain break please do so without hesitation.

## What if I Feel Threatened or Harassed While Handling a Support Request?
Just as Support Team are expected to adhere to the Code of Conduct, we also expect customers to treat the Support Team
with the same level of respect.

If you receive threatening or hostile emails from a user, please create a confidential issue in the GitLab Support Issue Tracker
and include:

1. A history of the user's communication
1. Relevant links
1. Summary of the high-level issues
1. Any other supporting information

Include your Manager, Director of Support, Abuse Team and Chief Culture Officer in this issue. Together we will evaluate on a
case-by-case basis whether to take action (e.g. ban the user from the forums for violating the [Code of Conduct](/handbook/people-operations/code-of-conduct)).

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Support Resources

- [Support Onboarding](/handbook/support/onboarding)
   - [Support Engineer Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Support%20Engineering%20Bootcamp.md)
   - [GitLab.com Support Agent Onboarding Checklist](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/GitLab.com%20Support%20Agent.md)
- [All Support Workflows](/handbook/support/workflows)
   - [GitLab.com Support Workflows](/handbook/support/workflows/services)
   - [Support Engineering Workflows](/handbook/support/workflows/support-engineering)
   - [How to Work with Tickets](/handbook/support/workflows/shared/support_workflows/working-with-tickets.html)
   - [How to Submit issues to Product/Development](/handbook/support/workflows/services/support_workflows/issue_escalations.html)
   - [How to Submit Code to the GitLab Application](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
   - [How to Submit Docs when working on customer issues](/handbook/documentation) (see 'docs-first methodology')
- [Support Hiring &amp; Managers](/handbook/support/managers)
- [Support Channels](/handbook/support/channels)
- [On-Call](/handbook/on-call/)
- [Support Ops](/handbook/support/support-ops)
- [Advanced Topics](/handbook/support/advanced-topics)
