---
layout: job_family_page
title: "Engineering Management - Quality"
---

# Quality Engineering Management Roles at GitLab

At GitLab Quality is every Engineer's responsibility and the output of the Quality department is instrumental to GitLab’s success.


Engineering Managers in the Quality department are motivated and experienced leaders who grow our test automation efforts across the entire GitLab ecosystem. 
They demonstrate a passion for high quality software, strong Engineering principles and methodical problem solving skills.

They own the delivery of test automation tools and strategy and are always looking to improve Engineering productivity. They also coordinate across departments and teams to accomplish collaborative goals.


## Quality Engineering Manager

### Responsibilities

* Drive improvements on test framework architecture and test coverage.
* Hire a world class team of Test Automation Engineers (SDETs) to work on their team.
* Help Test Automation/Engineering Productivity Engineers grow their skills and experience.
* Recommend improvements into overall best practices, design, testability and quality.
* Hold regular 1:1's with all members their team.
* Create a sense of psychological safety on their team.
* Excellent written and verbal communication skills.
* Define the team's road map and author project plans to deliver against that roadmap.
* Draft team's quarterly OKRs.
* Manages the team's priorities.
* Give clear, timely, and actionable feedback.
* Track quality, test metrics and test gaps. Work with other Engineering teams to incorporate lessons learned from this data.
* Strong sense of ownership, urgency, and drive.
* Excellent written and verbal communication skills, especially experience with executive-level communications.
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge.

### Requirements

* 8+ years of experience in the Quality Engineering/Engineering Productivity field.
* 2+ years of experience managing a team of Test Automation Engineers (SDETs) or Engineering Productivity Engineers.
* Strong experience with UI & API test automation tools, particularly in Ruby stack.
* Experience defining high-level test automation strategy based on DevOps industry's best practices.
* Experience driving organizational change with cross-functional stakeholders.
* Enterprise software company experience.
* Computer science education or equivalent experience.
* Passionate about open source, developer tools and shipping high-quality software.
* Collaborative team spirit with great communication skills
* You share our [values](/handbook/values), and work in accordance with those values.

### Interview Process

* [Screening call](/handbook/hiring/#screening-call) with a recruiter
* Interview with the Director of Quality (manager)
* Interview with a Sr. Test Automation Engineer (report)
* Interview with a Engineering Manager, Quality (peer)
* Interview with VP of Engineering

## Director of Quality Engineering

The Director of Quality Engineering role extends the [Engineering Manager, Quality](#engineering-manager-quality) role.

### Responsibilities

* Define and own all of GitLab's Quality Engineering, Test Automation, and Engineering Productivity.
* Set an ambitious vision for the department, product, and company.
* Manage the department's budget.
* Hire world class Engineering Managers/Engineering Managers in Test for the Quality department.
* Manage multiple teams and projects (Quality / Engineering Productivity).
* Drive the department's roadmap and quarterly OKRs.
* Track, publish and socialize the progress of the department.
* Drive improvements and process that impacts Quality.
* Manage the department’s priorities, set guidelines for Quality Engineering teams and align them strategically across all of Engineering.
* Work across the company to define and implement mechanisms to bake in Quality and testing earlier in the software development process.
* Exquisite written and verbal communication skills. Able to convey bottom-line messages to executives at C & VP level.
* Hold regular skip-level 1:1's with all members of their team.
* Help the teams in Quality grow their skills and experience, ensure that they are happy and productive.
* Interface with GitLab customers to address Quality and Performance gaps.
* Research and advice team on new technology and tools in the Quality Engineering space.
* Represent the company publicly at conferences.

### Requirements

* 12+ years of experience in the Quality Engineering/Engineering Productivity field.
* 2+ years of experience managing multiple teams of Test Automation Engineers (SDETs) or Engineering Productivity Engineers.
* This position does not require extensive Ruby development experience but the candidate should be very familiar with 
industry standard libraries (test tooling, orchestration, performance, and etc) and able to provide feedback on these topics.
* Experience driving the adoption of the DevOps across the Engineering organization (test pyramid, shifting left, and etc).
* Experience leading Quality focused SLA driven metrics across the Engineering organization (time to resolve bugs, time to merge code, and etc).
* Experience presenting at conferences and meet-ups in the Quality Engineering space.
* Proven track record of shipping multiple iterations of an Enterprise Product.
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. 
To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

* A short questionnaire from our Recruiting team
* Selected candidates will be invited to schedule a 30 minute screening call with our Recruiting team
* Next, candidates will then be invited to schedule a 1 hour technical interview with a Staff Engineer from the Quality department.
* Next, candidates, candidates will then be invited to schedule a 45 min interview with two Sr Engineers from the Quality department.
* Next, candidates will then be invited to schedule a 1 hour interview with an Engineering Manager, Quality.
* Next, candidates will be invited to schedule a 45 minute interview with the Director of Quality.
* Next, candidates will be invited to schedule a third 45 minute interview with our VP of Engineering.
* Finally, candidates maybe invited to a 45 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email

#### Interview Process

- [Screening call](/handbook/hiring/#screening-call) with a recruiter
- Interview with an Engineering Manager, Quality
- Interview with a Staff Engineer, Quality
- Interview with an Engineering Director (peer)
- Interview with VP of Engineering
- Interview with CEO

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
