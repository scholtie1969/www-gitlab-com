/* eslint-env jasmine */
/* eslint-disable global-require */
/* global Promise */

describe('the salary calculator', function() {
  var jsdom = require('jsdom');
  var path = require('path');
  var template = require('fs').readFileSync(path.resolve(__dirname, '../../source/includes/salary_calculator_vue.html.erb'));
  var SalaryCalculator;

  var fakeData = {
    contractTypes: [{
      country: 'United States',
      employee_factor: 1,
      entity: 'GitLab Inc'
    }, {
      country: '*',
      contractor_factor: 1.17,
      entity: 'GitLab BV'
    }],
    countryNoHire: [
      'Brazil'
    ],
    currencyExchangeRates: {
      rates_to_usd: [{
        currency_code: 'CAD',
        rate: 0.76088,
        countries: ['Canada']
      }]
    },
    numbeo: [{
      country: 'United States',
      area: 'Columbus, Ohio',
      locationFactor: 47.36
    }, {
      country: 'Canada',
      area: 'Alberta',
      locationFactor: 36.5
    }],
    roles: [{
      title: 'Backend Engineer',
      levels: 'engineering_ic',
      salary: 160000
    }, {
      title: 'Engineering Management, Quality',
      levels: false,
      salary: 182563
    }],
    roleLevels: {
      engineering_ic: [{
        title: 'Junior',
        factor: 0.8
      }, {
        title: 'Intermediate',
        factor: 1.0,
        is_default: true
      }]
    }
  };


  beforeEach(function() {
    var dom = new jsdom.JSDOM(template);
    var jQuery = require('../../source/javascripts/libs/jquery.min')(dom.window);
    var promise = Promise.resolve(fakeData);

    global.document = dom.window.document;
    global.navigator = dom.window.navigator;
    global.window = dom.window;
    global.window.Vue = require('../../source/javascripts/libs/vue.min');
    global.window.VueSelect = require('../../source/javascripts/libs/vue-select.min');
    global.window.$ = jQuery;

    spyOn(jQuery, 'get').and.callFake(function() {
      return promise;
    });

    SalaryCalculator = require('../../source/javascripts/salary-calculator-vue');

    promise.then(function() {
      SalaryCalculator.currentRole = fakeData.roles[0];
      SalaryCalculator.setRoleLevels();
    });

    return promise;
  });

  describe('methods', function() {
    describe('setRoleLevels', function() {
      it('sets the default level if none are available', function() {
        SalaryCalculator.currentRole = fakeData.roles[1];
        SalaryCalculator.setRoleLevels();

        expect(SalaryCalculator.currentLevel).toEqual({ title: 'N/A', factor: 1 });
      });

      it('sets the current level from the role, if available', function() {
        SalaryCalculator.currentRole = fakeData.roles[0];
        SalaryCalculator.setRoleLevels();

        expect(SalaryCalculator.currentLevel).toEqual(fakeData.roleLevels.engineering_ic[1]);
      });
    });

    describe('formatAmount', function() {
      it('formats the amount in USD by default', function() {
        expect(SalaryCalculator.formatAmount(1234)).toEqual('$1,234');
      });

      it('formats the amount in another currency code when passed', function() {
        expect(SalaryCalculator.formatAmount(1234, 'AUD')).toEqual('1,234 AUD');
      });
    });

    describe('formatArea', function() {
      it('formats the number divided by 100 to three decimal places', function() {
        expect(SalaryCalculator.formatArea({ locationFactor: 123 })).toEqual('1.230');
      });
    });

    describe('findByCountry', function() {
      it('finds a result matching the country name', function() {
        expect(SalaryCalculator.findByCountry(fakeData.contractTypes, 'United States')).toEqual(fakeData.contractTypes[0]);
      });

      it('falls back to * when there is no match', function() {
        expect(SalaryCalculator.findByCountry(fakeData.contractTypes, 'Somewhere else')).toEqual(fakeData.contractTypes[1]);
      });
    });
  });
});
