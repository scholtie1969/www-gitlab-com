# Note that the rspec job below uses a different image that also
# includes chromedriver. If we update the Ruby version for this image,
# we should also update it for the rspec job.
image: dev.gitlab.org:5005/gitlab/gitlab-build-images:www-gitlab-com-2.4

variables:
  GIT_DEPTH: "10"
  # Speed up middleman
  NO_CONTRACTS: "true"

.install: &install
  bundle install --jobs 4 --path vendor

before_script: [*install]

cache:
  key: "web_ruby-2.4.4"
  paths:
    - tmp/cache
    - vendor

stages:
  - prepare
  - build
  - deploy

lint 0 2:
  stage: build
  script:
    - bundle exec rake lint
  tags:
    - gitlab-org

lint 1 2:
  cache: {}
  before_script: []
  stage: build
  script:
    - yarn install
    - yarn run eslint
    - yarn run yamllint
  tags:
    - gitlab-org

crop_pictures:
  cache: {}
  before_script: []
  stage: prepare
  script:
    - bin/crop-team-pictures
  artifacts:
    paths:
      - data/team.yml
      - data/pets.yml
      - source/images/team/
      - source/community/alumni/index.html.haml
  tags:
    - gitlab-org

pngbot_commit:
  image: registry.gitlab.com/jramsay/pngbot:v0.1.0
  before_script: []
  cache: {}
  except:
    - master@gitlab-com/www-gitlab-com
  only:
    - branches@gitlab-com/www-gitlab-com
  stage: prepare
  script:
    - pngbot
  tags:
    - gitlab-org

rubocop:
  stage: build
  script:
    - bundle exec rubocop
  tags:
    - gitlab-org

.rspec_job: &rspec_job
  image: dev.gitlab.org:5005/gitlab/gitlab-build-images:ruby-2.4.5-git-2.18-chrome-69.0-docker-18.06.1
  stage: build
  script:
    - bundle exec rspec
  tags:
    - gitlab-org

spec 0 2:
  <<: *rspec_job
  allow_failure: true
  only:
    refs:
      - master

spec 0 2:
  <<: *rspec_job
  allow_failure: false
  except:
    refs:
      - master

spec 1 2:
  cache: {}
  before_script: []
  stage: build
  script:
    - yarn install
    - yarn run test
  tags:
    - gitlab-org

enforce_relative_links:
  stage: build
  image: alpine
  allow_failure: true
  cache: {}
  before_script:
    - apk add --update the_silver_searcher
  script:
    - set +o errexit
    - ag --filename --numbers --break --nogroup --depth -1 --stats --path-to-ignore ./.relative_links_ignore '(?<!`|")https?://about.gitlab.com(?!`|\S*")' ./source && rc="$?" || rc="$?"
    - if [ "$rc" -eq 0 ]; then exit 1; else exit 0; fi
  tags:
    - gitlab-org

check_links:
  before_script: []
  image: coala/base
  stage: build
  script:
    - git fetch --unshallow && git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" && git fetch origin master
    - git diff --numstat origin/master..$CI_COMMIT_REF_NAME -- | awk '/(.+\.md)|(.+\.haml)/ { print $3 }' > new_files
    - coala --no-config --ci --bears InvalidLinkBear --settings follow_redirects=True --files="$(paste -s -d, new_files)"
  when: manual
  allow_failure: true
  except:
    - master
  tags:
    - gitlab-org

generate-handbook-changelog:
  stage: build
  script:
    - bundle exec bin/generate_handbook_changelog
  only:
    - schedules
  tags:
    - gitlab-org

.build_base: &build_base
  before_script:
    - find source/images/team -type f ! -name '*-crop.jpg' -delete
    - *install
  stage: build
  dependencies:
    - crop_pictures
  artifacts:
    expire_in: 7 days
    paths:
      - public/
      - source/images/team/
  tags:
    - gitlab-org

build_branch:
  <<: *build_base
  script:
    - bundle exec rake build
  except:
    - master

build_master:
  <<: *build_base
  variables:
    MIDDLEMAN_ENV: 'production'
  script:
    - bundle exec rake build pdfs
  only:
    - master

codequality:
  stage: build
  image: docker:stable
  allow_failure: true
  before_script: []
  cache: {}
  dependencies: []
  tags: []
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SOURCE_CODE="$PWD"
        --volume "$PWD":/code
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  artifacts:
    paths:
      - coffeelint.json
      - gl-code-quality-report.json

dependency_scanning:
  stage: build
  image: docker:stable
  allow_failure: true
  before_script: []
  cache: {}
  dependencies: []
  tags: []
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json

review:
  stage: deploy
  allow_failure: true
  before_script: []
  cache: {}
  dependencies:
    - build_branch
  variables:
    GIT_STRATEGY: none
  script:
    # We sometimes have absolute URLs, this replaces them with correct ones for the review app
    - >
      find public/ -regextype egrep -iregex ".*\.(html|js|css|json|xml|txt)" -exec \
        sed --in-place "s#https\?://about.gitlab.com#https://$CI_COMMIT_REF_SLUG.about-src.gitlab.com#g" "{}" +;
    - rsync --ignore-times --checksum --delete -avz public ~/pages/$CI_COMMIT_REF_SLUG
    - rm -rf ./public/
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.about-src.gitlab.com
    on_stop: review_stop
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  tags:
    - deploy
    - review-apps

review_stop:
  stage: deploy
  before_script: []
  artifacts: {}
  cache: {}
  dependencies: []
  variables:
    GIT_STRATEGY: none
  script:
    - rm -rf ~/pages/$CI_COMMIT_REF_SLUG
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  only:
    - branches@gitlab-com/www-gitlab-com
  except:
    - master@gitlab-com/www-gitlab-com
  tags:
    - deploy
    - review-apps

deploy:
  stage: deploy
  cache: {}
  variables:
    GIT_STRATEGY: none
  dependencies:
    - build_master
  before_script: []
  script:
    - rsync --ignore-times --checksum --delete -avz public/ ~/public/
    - rm -rf ./public/
  environment:
    name: production
    url: https://about.gitlab.com
  tags:
    - deploy
  only:
    - master@gitlab-com/www-gitlab-com
